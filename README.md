# Hangprinter Flex Compensation

![Flex compensation with four anchors at z=0 height](flexcomp2.png)
![Flex compensation with five anchors at z=0 height](flexcomp_QuadPyramid2.png)

The image shows the effect of flex compensation when moving around in the xy-plane at z=0. Bluer/lower areas signify tighter lines. Redder/higher areas are where the compensation algorithm want to slack the lines. A minimum limit on line tension makes the red areas flatten out. A maximum limit on line tension makes the blue areas look flat although tilted in some areas.

The exact shape of the compensation varies depending on configuration values for

 - Number of anchors
 - Mover weight
 - Line stiffness
 - Anchor locations
 - Minimum line tension
 - Maximum line tension
 - Target tension

## The Algorithm

The general idea for now is to find out a set of four/five forces that
 - Counteracts gravity while keeping mover in place
 - Tries to fulfill the (minimum) target force requirement, unless that would break a max force limit
 - Enforces max force limits while retaining positional control

We approximate lines to be linear springs when we translate from forces to flex-distances. We subtract flex-distance from each line length to obtain flex-compensated line-lengths.

## How to use
The image above was created with

```
./plotstuff.sh
```

There are some test you can compile and run also.

```
./test.sh
```
