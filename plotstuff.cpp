#include <array>
#include <cmath>
#include <iostream>
#include <vector>

#include <flex.hpp>

int main() {
  size_t constexpr numAnchors = 5;
  size_t constexpr numPlots = 5;
  Flex const flex{numAnchors};
  flex.PrintPretension();
  // return 0;

  std::array<float, HANGPRINTER_MAX_ANCHORS> motorPos = {0};

  // float const y{0.0};
  // float const min{-80.0};
  // float const max{81.0};
  float const z{0.0};
  float const min{-1611.0}; // Same as A anchor y position
  float const max{1601.0};
  float const step{35.0};
  float const steps{std::floor((max - min) / step)};

  std::vector<Point> positions{};
  std::vector<std::vector<float>> X{};
  std::vector<std::vector<float>> Y{};
  std::array<std::vector<std::vector<float>>, numPlots> Z{};
  positions.reserve(steps * steps);
  // for (float z{0}; z < 2500; z += step) {
  for (float y{min}; y < max; y += step) {
    std::vector<float> xl{};
    std::vector<float> yl{};
    std::array<std::vector<float>, numPlots> zl{};
    for (float x{min}; x < max; x += step) {
      xl.push_back(x);
      // yl.push_back(z);
      yl.push_back(y);
      auto tmp = flex.CartesianToMotorStepsMatrix({x, y, z}, motorPos);
      for (size_t i{0}; i < numPlots; ++i) {
        zl[i].push_back(tmp[i]);
      }
    }
    X.push_back(xl);
    Y.push_back(yl);
    for (size_t i{0}; i < numPlots; ++i) {
      Z[i].push_back(zl[i]);
    }
  }

  std::cout << "from mpl_toolkits.mplot3d import axes3d\nimport matplotlib.pyplot as plt\nfrom matplotlib import cm\nimport numpy as "
               "np\n\n";

  std::array<char, HANGPRINTER_MAX_ANCHORS> letters = {'A', 'B', 'C', 'D', 'I'};

  for (size_t i{0}; i < numPlots; ++i) {
    std::cout << "ax" << i << " = plt.figure().add_subplot(111, projection='3d', title='" << letters[i] << "-lines')\n";
  }
  std::cout << '\n';

  std::cout << "X = np.array([";
  std::string delim0 = "";
  for (auto const &xl : X) {
    std::cout << delim0 << "[";
    delim0 = "],\n              ";
    std::string delim1 = "";
    for (auto const &x : xl) {
      std::cout << delim1;
      std::cout << x;
      delim1 = ", ";
    }
  }
  std::cout << "]])\n";

  std::cout << "Y = np.array([";
  delim0 = "";
  for (auto const &yl : Y) {
    std::cout << delim0 << "[";
    delim0 = "],\n              ";
    std::string delim1 = "";
    for (auto const &y : yl) {
      std::cout << delim1;
      std::cout << y;
      delim1 = ", ";
    }
  }
  std::cout << "]])\n";

  for (size_t i{0}; i < numPlots; ++i) {
    std::cout << 'Z' << i << " = np.array([";
    delim0 = "";
    for (auto const &zl : Z[i]) {
      std::cout << delim0 << "[";
      delim0 = "],\n              ";
      std::string delim1 = "";
      for (auto const &z : zl) {
        std::cout << delim1;
        std::cout << z;
        delim1 = ", ";
      }
    }
    std::cout << "]])\n\n";
  }

  for (size_t i{0}; i < numPlots; ++i) {
    std::cout << "surf" << i << " = ax" << i << ".plot_surface(X, Y, Z" << i << ", cmap=cm.coolwarm)\n\n";
  }

  std::cout << "\nplt.show()\n";

  return 0;
}
