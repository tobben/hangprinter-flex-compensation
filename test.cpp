#include <array>
#include <cmath>
#include <iostream>
#include <vector>

#include <flex.hpp>
#include <ut.hpp>
#include <util.hpp>

boost::ut::suite tests = [] {
  using namespace boost::ut;
  std::array<ssize_t, HANGPRINTER_MAX_ANCHORS> motorPos = { 0 };

  "with mass=0"_test = [&] {
    Flex const flex{4, 0.0};
    for (float x{-1000.0}; x < 1001.0; x += 500.0) {
      for (float y{-1000.0}; y < 1001.0; y += 500.0) {
        for (float z{-1000.0}; z < 1001.0; z += 500.0) {
          auto const flexDiff = flex.CartesianToMotorSteps({x, y, z}, motorPos);
          expect((flexDiff[0] == 0.00000_d) >> fatal) << "Mover weight 0 should shut off flex compensation";
          expect(flexDiff[1] == 0.00000_d);
          expect(flexDiff[2] == 0.00000_d);
          expect(flexDiff[3] == 0.00000_d);
          auto const flexDiffMatrix = flex.CartesianToMotorStepsMatrix({x, y, z}, motorPos);
          expect(flexDiff[0] == flexDiffMatrix[0]);
          expect(flexDiff[1] == flexDiffMatrix[1]);
          expect(flexDiff[2] == flexDiffMatrix[2]);
          expect(flexDiff[3] == flexDiffMatrix[3]);
        }
      }
    }
  };

  "at origin"_test = [&] {
    Flex const flex{4};
    auto flexDiff = flex.CartesianToMotorSteps({0, 0, 0}, motorPos);
    expect(flexDiff[0] == 0.00000_d) << "Enabling flex compensation at the origin should not cause motors to move";
    expect(flexDiff[1] == 0.00000_d);
    expect(flexDiff[2] == 0.00000_d);
    expect(flexDiff[3] == 0.00000_d);
    expect(motorPos[0] == 0_ul);
    expect(motorPos[1] == 0_ul);
    expect(motorPos[2] == 0_ul);
    expect(motorPos[3] == 0_ul);
    auto const flexDiffMatrix = flex.CartesianToMotorStepsMatrix({0, 0, 0}, motorPos);
    expect(eq(flexDiff[0], flexDiffMatrix[0]));
    expect(std::abs(flexDiff[1] - flexDiffMatrix[1]) == 0.000_d);
    expect(eq(flexDiff[2], flexDiffMatrix[2]));
    expect(eq(flexDiff[3], flexDiffMatrix[3]));
  };

  "at ax=0"_test = [&] {
    Flex const flex{
        std::array<Point, 4>{{{17.4, -1610.98, -131.53}, {1314.22, 1268.14, -121.28}, {-1415.73, 707.61, -121.82}, {0.0, 0.0, 2299.83}}}};

    auto const flexDiffLeft = flex.CartesianToMotorSteps({16.3, 0, 0}, motorPos);
    auto const flexDiffAt = flex.CartesianToMotorSteps({16.4, 0, 0}, motorPos);
    auto const flexDiffRight = flex.CartesianToMotorSteps({16.5, 0, 0}, motorPos);
    expect(flexDiffLeft[0] != 0.000_d);
    expect(flexDiffLeft[1] != 0.000_d);
    expect(flexDiffLeft[2] != 0.000_d);

    expect(std::abs(flexDiffLeft[0] - flexDiffAt[0]) < 0.01_d) << "We expect tiny differences in ABC";
    expect(std::abs(flexDiffLeft[1] - flexDiffAt[1]) < 0.01_d);
    expect(std::abs(flexDiffLeft[2] - flexDiffAt[2]) < 0.01_d);
    expect(std::abs(flexDiffLeft[3] - flexDiffAt[3]) < 0.01_d);
    expect(std::abs(flexDiffLeft[0] - flexDiffAt[0]) >= 0.000_d);
    expect(std::abs(flexDiffLeft[1] - flexDiffAt[1]) > 0.0001_d);
    expect(std::abs(flexDiffLeft[2] - flexDiffAt[2]) > 0.0001_d);

    expect(std::abs(flexDiffRight[0] - flexDiffAt[0]) < 0.01_d);
    expect(std::abs(flexDiffRight[1] - flexDiffAt[1]) < 0.01_d);
    expect(std::abs(flexDiffRight[2] - flexDiffAt[2]) < 0.01_d);
    expect(std::abs(flexDiffRight[3] - flexDiffAt[3]) < 0.01_d);
    expect(std::abs(flexDiffRight[0] - flexDiffAt[0]) > 0.0001_d);
    expect(std::abs(flexDiffRight[1] - flexDiffAt[1]) > 0.0001_d);
    // expect(std::abs(flexDiffRight[2] - flexDiffAt[2]) > 0.0001_d);

    expect(std::abs(flexDiffLeft[0] - flexDiffRight[0]) < 0.01_d);
    expect(std::abs(flexDiffLeft[1] - flexDiffRight[1]) < 0.01_d);
    expect(std::abs(flexDiffLeft[2] - flexDiffRight[2]) < 0.01_d);
    expect(std::abs(flexDiffLeft[3] - flexDiffRight[3]) < 0.01_d);
    expect(std::abs(flexDiffLeft[0] - flexDiffRight[0]) > 0.0001_d);
    expect(std::abs(flexDiffLeft[1] - flexDiffRight[1]) > 0.0001_d);
    expect(std::abs(flexDiffLeft[2] - flexDiffRight[2]) > 0.0001_d);

    auto const flexDiffMatrixLeft = flex.CartesianToMotorStepsMatrix({16.3, 0, 0}, motorPos);
    auto const flexDiffMatrixAt = flex.CartesianToMotorStepsMatrix({16.4, 0, 0}, motorPos);
    auto const flexDiffMatrixRight = flex.CartesianToMotorStepsMatrix({16.5, 0, 0}, motorPos);
    expect(std::abs(flexDiffLeft[0] - flexDiffMatrixLeft[0]) < 0.001_d);
    expect(std::abs(flexDiffLeft[1] - flexDiffMatrixLeft[1]) < 0.001_d);
    expect(std::abs(flexDiffLeft[2] - flexDiffMatrixLeft[2]) < 0.001_d);
    expect(std::abs(flexDiffLeft[3] - flexDiffMatrixLeft[3]) < 0.001_d);
    expect(std::abs(flexDiffAt[0] - flexDiffMatrixAt[0]) < 0.001_d);
    expect(std::abs(flexDiffAt[1] - flexDiffMatrixAt[1]) < 0.001_d);
    expect(std::abs(flexDiffAt[2] - flexDiffMatrixAt[2]) < 0.001_d);
    expect(std::abs(flexDiffAt[3] - flexDiffMatrixAt[3]) < 0.001_d);
    expect(std::abs(flexDiffRight[0] - flexDiffMatrixRight[0]) < 0.001_d);
    expect(std::abs(flexDiffRight[1] - flexDiffMatrixRight[1]) < 0.001_d);
    expect(std::abs(flexDiffRight[2] - flexDiffMatrixRight[2]) < 0.001_d);
    expect(std::abs(flexDiffRight[3] - flexDiffMatrixRight[3]) < 0.001_d);
  };

  "at problem point2"_test = [&] {
    Flex const flex{
        std::array<Point, 4>{{{-1000.0, 1000.0, -121.28}, {500.0, -500.0, -131.53}, {-1415.73, 707.61, -121.82}, {0.0, 0.0, 2299.83}}}};

    auto flexDiffLeft = flex.CartesianToMotorSteps({-0.1, 0, 0}, motorPos);
    auto flexDiffAt = flex.CartesianToMotorSteps({0.0, 0, 0}, motorPos);
    auto flexDiffRight = flex.CartesianToMotorSteps({0.1, 0, 0}, motorPos);

    expect(std::abs(flexDiffLeft[0] - flexDiffAt[0]) < 10.0_d) << "We expect nothing to explode near the singularity";
    expect(std::abs(flexDiffLeft[1] - flexDiffAt[1]) < 10.0_d);
    expect(std::abs(flexDiffLeft[2] - flexDiffAt[2]) < 10.0_d);
    expect(std::abs(flexDiffLeft[3] - flexDiffAt[3]) < 10.0_d);
    expect(std::abs(flexDiffLeft[0] - flexDiffAt[0]) > 0.0001_d);
    expect(std::abs(flexDiffLeft[1] - flexDiffAt[1]) > 0.0001_d);
    expect(std::abs(flexDiffLeft[2] - flexDiffAt[2]) > 0.0001_d);

    expect(std::abs(flexDiffRight[0] - flexDiffAt[0]) < 10.0_d) << "We expect nothing to explode near the singularity";
    expect(std::abs(flexDiffRight[1] - flexDiffAt[1]) < 10.0_d);
    expect(std::abs(flexDiffRight[2] - flexDiffAt[2]) < 10.0_d);
    expect(std::abs(flexDiffRight[3] - flexDiffAt[3]) < 10.0_d);
    expect(std::abs(flexDiffRight[0] - flexDiffAt[0]) > 0.0001_d);
    expect(std::abs(flexDiffRight[1] - flexDiffAt[1]) > 0.0001_d);
    // expect(std::abs(flexDiffRight[2] - flexDiffAt[2]) > 0.0001_d); // Don't know why this is exactly 0 but ok

    expect(std::abs(flexDiffLeft[0] - flexDiffRight[0]) < 10.0_d) << "We expect nothing to explode across the singlularity";
    expect(std::abs(flexDiffLeft[1] - flexDiffRight[1]) < 10.0_d);
    expect(std::abs(flexDiffLeft[2] - flexDiffRight[2]) < 10.0_d);
    expect(std::abs(flexDiffLeft[3] - flexDiffRight[3]) < 10.0_d);
    expect(std::abs(flexDiffLeft[0] - flexDiffRight[0]) > 0.0001_d);
    expect(std::abs(flexDiffLeft[1] - flexDiffRight[1]) > 0.0001_d);
    expect(std::abs(flexDiffLeft[2] - flexDiffRight[2]) > 0.0001_d);

    auto const flexDiffMatrixLeft = flex.CartesianToMotorStepsMatrix({-0.1, 0, 0}, motorPos);
    auto const flexDiffMatrixAt = flex.CartesianToMotorStepsMatrix({0, 0, 0}, motorPos);
    auto const flexDiffMatrixRight = flex.CartesianToMotorStepsMatrix({0.1, 0, 0}, motorPos);

    // std::cout << flexDiffLeft << '\n';
    // std::cout << flexDiffAt << '\n';
    // std::cout << flexDiffRight << '\n';
    // std::cout << flexDiffMatrixLeft << '\n';
    // std::cout << flexDiffMatrixAt << '\n';
    // std::cout << flexDiffMatrixRight << '\n';

    expect(eq(flexDiffLeft[0], flexDiffMatrixLeft[0])) << "We expect our two Gauss elimination methods to yield similar results";
    expect(eq(flexDiffLeft[1], flexDiffMatrixLeft[1]));
    expect(eq(flexDiffLeft[2], flexDiffMatrixLeft[2]));
    expect(eq(flexDiffLeft[3], flexDiffMatrixLeft[3]));
    expect(eq(flexDiffAt[0], flexDiffMatrixAt[0]));
    expect(eq(flexDiffAt[1], flexDiffMatrixAt[1]));
    expect(eq(flexDiffAt[2], flexDiffMatrixAt[2]));
    expect(eq(flexDiffAt[3], flexDiffMatrixAt[3]));
    expect(eq(flexDiffRight[0], flexDiffMatrixRight[0]));
    expect(eq(flexDiffRight[1], flexDiffMatrixRight[1]));
    expect(eq(flexDiffRight[2], flexDiffMatrixRight[2]));
    expect(eq(flexDiffRight[3], flexDiffMatrixRight[3]));
  };
};

boost::ut::suite five_anchor_tests = [] {
  using namespace boost::ut;
  std::array<ssize_t, HANGPRINTER_MAX_ANCHORS> motorPos = { 0 };

  "5:with mass=0"_test = [&] {
    Flex const flex{5, 0.0};
    for (float x{-1000.0}; x < 1001.0; x += 500.0) {
      for (float y{-1000.0}; y < 1001.0; y += 500.0) {
        for (float z{-1000.0}; z < 1001.0; z += 500.0) {
          auto const flexDiff = flex.CartesianToMotorSteps({x, y, z}, motorPos);
          expect((flexDiff[0] == 0.00000_d) >> fatal) << "Mover weight 0 should shut off flex compensation";
          expect(flexDiff[1] == 0.00000_d);
          expect(flexDiff[2] == 0.00000_d);
          expect(flexDiff[3] == 0.00000_d);
          expect(flexDiff[4] == 0.00000_d);
          auto const flexDiffMatrix = flex.CartesianToMotorStepsMatrix({x, y, z}, motorPos);
          expect(flexDiff[0] == flexDiffMatrix[0]);
          expect(flexDiff[1] == flexDiffMatrix[1]);
          expect(flexDiff[2] == flexDiffMatrix[2]);
          expect(flexDiff[3] == flexDiffMatrix[3]);
          expect(flexDiff[4] == flexDiffMatrix[4]);
        }
      }
    }
  };

  "5:at origin"_test = [&] {
    Flex const flex{5};
    auto flexDiff = flex.CartesianToMotorSteps({0, 0, 0}, motorPos);
    expect(flexDiff[0] == 0.00000_d) << "Enabling flex compensation at the origin should not cause motors to move";
    expect(flexDiff[1] == 0.00000_d);
    expect(flexDiff[2] == 0.00000_d);
    expect(flexDiff[3] == 0.00000_d);
    expect(flexDiff[4] == 0.00000_d);
    expect(motorPos[0] == 0_ul);
    expect(motorPos[1] == 0_ul);
    expect(motorPos[2] == 0_ul);
    expect(motorPos[3] == 0_ul);
    expect(motorPos[4] == 0_ul);
    auto const flexDiffMatrix = flex.CartesianToMotorStepsMatrix({0, 0, 0}, motorPos);
    expect(eq(flexDiff[0], flexDiffMatrix[0]));
    expect(std::abs(flexDiff[1] - flexDiffMatrix[1]) == 0.000_d);
    expect(eq(flexDiff[2], flexDiffMatrix[2]));
    expect(eq(flexDiff[3], flexDiffMatrix[3]));
    expect(eq(flexDiff[4], flexDiffMatrix[4]));
  };

  "5:at ax=0"_test = [&] {
    Flex const flex{
      std::array<Point, 5>{{{16.4, -1620.98, -131.53},
                           {1314.22, 128.14, -121.28},
                           {-15.73, 1415.61, -121.82},
                           {-1211.62, 18.14, -111.18},
                           {0.0, 0.0, 2299.83}}}};

      auto const flexDiffLeft = flex.CartesianToMotorSteps({16.3, 0, 0}, motorPos);
      //auto const flexDiffAt = flex.CartesianToMotorSteps({16.4, 0, 0}, motorPos);
      //auto const flexDiffRight = flex.CartesianToMotorSteps({16.5, 0, 0}, motorPos);
      //expect(flexDiffLeft[0] != 0.000_d);
      //expect(flexDiffLeft[1] != 0.000_d);
      //expect(flexDiffLeft[2] != 0.000_d);

      //expect(std::abs(flexDiffLeft[0] - flexDiffAt[0]) < 0.01_d) << "We expect tiny differences in low anchors";
      //expect(std::abs(flexDiffLeft[1] - flexDiffAt[1]) < 0.01_d);
      //expect(std::abs(flexDiffLeft[2] - flexDiffAt[2]) < 0.01_d);
      //expect(std::abs(flexDiffLeft[3] - flexDiffAt[3]) < 0.01_d);
      //expect(std::abs(flexDiffLeft[4] - flexDiffAt[4]) < 0.01_d);
      //expect(std::abs(flexDiffLeft[0] - flexDiffAt[0]) >= 0.000_d);
      //expect(std::abs(flexDiffLeft[1] - flexDiffAt[1]) > 0.0001_d);
      //expect(std::abs(flexDiffLeft[2] - flexDiffAt[2]) > 0.0001_d);
      //expect(std::abs(flexDiffLeft[3] - flexDiffAt[3]) > 0.0001_d);
      //expect(std::abs(flexDiffLeft[4] - flexDiffAt[4]) >= 0.000_d);

      //expect(std::abs(flexDiffRight[0] - flexDiffAt[0]) < 0.01_d);
      //expect(std::abs(flexDiffRight[1] - flexDiffAt[1]) < 0.01_d);
      //expect(std::abs(flexDiffRight[2] - flexDiffAt[2]) < 0.01_d);
      //expect(std::abs(flexDiffRight[3] - flexDiffAt[3]) < 0.01_d);
      //expect(std::abs(flexDiffRight[0] - flexDiffAt[0]) > 0.0001_d);
      //expect(std::abs(flexDiffRight[1] - flexDiffAt[1]) > 0.0001_d);
      //// expect(std::abs(flexDiffRight[2] - flexDiffAt[2]) > 0.0001_d);

      //expect(std::abs(flexDiffLeft[0] - flexDiffRight[0]) < 0.01_d);
      //expect(std::abs(flexDiffLeft[1] - flexDiffRight[1]) < 0.01_d);
      //expect(std::abs(flexDiffLeft[2] - flexDiffRight[2]) < 0.01_d);
      //expect(std::abs(flexDiffLeft[3] - flexDiffRight[3]) < 0.01_d);
      //expect(std::abs(flexDiffLeft[0] - flexDiffRight[0]) > 0.0001_d);
      //expect(std::abs(flexDiffLeft[1] - flexDiffRight[1]) > 0.0001_d);
      //expect(std::abs(flexDiffLeft[2] - flexDiffRight[2]) > 0.0001_d);

      //auto const flexDiffMatrixLeft = flex.CartesianToMotorStepsMatrix({16.3, 0, 0}, motorPos);
      //auto const flexDiffMatrixAt = flex.CartesianToMotorStepsMatrix({16.4, 0, 0}, motorPos);
      //auto const flexDiffMatrixRight = flex.CartesianToMotorStepsMatrix({16.5, 0, 0}, motorPos);
      //expect(std::abs(flexDiffLeft[0] - flexDiffMatrixLeft[0]) < 0.001_d);
      //expect(std::abs(flexDiffLeft[1] - flexDiffMatrixLeft[1]) < 0.001_d);
      //expect(std::abs(flexDiffLeft[2] - flexDiffMatrixLeft[2]) < 0.001_d);
      //expect(std::abs(flexDiffLeft[3] - flexDiffMatrixLeft[3]) < 0.001_d);
      //expect(std::abs(flexDiffAt[0] - flexDiffMatrixAt[0]) < 0.001_d);
      //expect(std::abs(flexDiffAt[1] - flexDiffMatrixAt[1]) < 0.001_d);
      //expect(std::abs(flexDiffAt[2] - flexDiffMatrixAt[2]) < 0.001_d);
      //expect(std::abs(flexDiffAt[3] - flexDiffMatrixAt[3]) < 0.001_d);
      //expect(std::abs(flexDiffRight[0] - flexDiffMatrixRight[0]) < 0.001_d);
      //expect(std::abs(flexDiffRight[1] - flexDiffMatrixRight[1]) < 0.001_d);
      //expect(std::abs(flexDiffRight[2] - flexDiffMatrixRight[2]) < 0.001_d);
      //expect(std::abs(flexDiffRight[3] - flexDiffMatrixRight[3]) < 0.001_d);
  };

  //"at problem point2"_test = [&] {
  //  Flex const flex{
  //      std::array<Point, 4>{{{-1000.0, 1000.0, -121.28}, {500.0, -500.0, -131.53}, {-1415.73, 707.61, -121.82}, {0.0, 0.0, 2299.83}}}};

  //  auto flexDiffLeft = flex.CartesianToMotorSteps({-0.1, 0, 0}, motorPos);
  //  auto flexDiffAt = flex.CartesianToMotorSteps({0.0, 0, 0}, motorPos);
  //  auto flexDiffRight = flex.CartesianToMotorSteps({0.1, 0, 0}, motorPos);

  //  expect(std::abs(flexDiffLeft[0] - flexDiffAt[0]) < 10.0_d) << "We expect nothing to explode near the singularity";
  //  expect(std::abs(flexDiffLeft[1] - flexDiffAt[1]) < 10.0_d);
  //  expect(std::abs(flexDiffLeft[2] - flexDiffAt[2]) < 10.0_d);
  //  expect(std::abs(flexDiffLeft[3] - flexDiffAt[3]) < 10.0_d);
  //  expect(std::abs(flexDiffLeft[0] - flexDiffAt[0]) > 0.0001_d);
  //  expect(std::abs(flexDiffLeft[1] - flexDiffAt[1]) > 0.0001_d);
  //  expect(std::abs(flexDiffLeft[2] - flexDiffAt[2]) > 0.0001_d);

  //  expect(std::abs(flexDiffRight[0] - flexDiffAt[0]) < 10.0_d) << "We expect nothing to explode near the singularity";
  //  expect(std::abs(flexDiffRight[1] - flexDiffAt[1]) < 10.0_d);
  //  expect(std::abs(flexDiffRight[2] - flexDiffAt[2]) < 10.0_d);
  //  expect(std::abs(flexDiffRight[3] - flexDiffAt[3]) < 10.0_d);
  //  expect(std::abs(flexDiffRight[0] - flexDiffAt[0]) > 0.0001_d);
  //  expect(std::abs(flexDiffRight[1] - flexDiffAt[1]) > 0.0001_d);
  //  // expect(std::abs(flexDiffRight[2] - flexDiffAt[2]) > 0.0001_d); // Don't know why this is exactly 0 but ok

  //  expect(std::abs(flexDiffLeft[0] - flexDiffRight[0]) < 10.0_d) << "We expect nothing to explode across the singlularity";
  //  expect(std::abs(flexDiffLeft[1] - flexDiffRight[1]) < 10.0_d);
  //  expect(std::abs(flexDiffLeft[2] - flexDiffRight[2]) < 10.0_d);
  //  expect(std::abs(flexDiffLeft[3] - flexDiffRight[3]) < 10.0_d);
  //  expect(std::abs(flexDiffLeft[0] - flexDiffRight[0]) > 0.0001_d);
  //  expect(std::abs(flexDiffLeft[1] - flexDiffRight[1]) > 0.0001_d);
  //  expect(std::abs(flexDiffLeft[2] - flexDiffRight[2]) > 0.0001_d);

  //  auto const flexDiffMatrixLeft = flex.CartesianToMotorStepsMatrix({-0.1, 0, 0}, motorPos);
  //  auto const flexDiffMatrixAt = flex.CartesianToMotorStepsMatrix({0, 0, 0}, motorPos);
  //  auto const flexDiffMatrixRight = flex.CartesianToMotorStepsMatrix({0.1, 0, 0}, motorPos);

  //  // std::cout << flexDiffLeft << '\n';
  //  // std::cout << flexDiffAt << '\n';
  //  // std::cout << flexDiffRight << '\n';
  //  // std::cout << flexDiffMatrixLeft << '\n';
  //  // std::cout << flexDiffMatrixAt << '\n';
  //  // std::cout << flexDiffMatrixRight << '\n';

  //  expect(eq(flexDiffLeft[0], flexDiffMatrixLeft[0])) << "We expect our two Gauss elimination methods to yield similar results";
  //  expect(eq(flexDiffLeft[1], flexDiffMatrixLeft[1]));
  //  expect(eq(flexDiffLeft[2], flexDiffMatrixLeft[2]));
  //  expect(eq(flexDiffLeft[3], flexDiffMatrixLeft[3]));
  //  expect(eq(flexDiffAt[0], flexDiffMatrixAt[0]));
  //  expect(eq(flexDiffAt[1], flexDiffMatrixAt[1]));
  //  expect(eq(flexDiffAt[2], flexDiffMatrixAt[2]));
  //  expect(eq(flexDiffAt[3], flexDiffMatrixAt[3]));
  //  expect(eq(flexDiffRight[0], flexDiffMatrixRight[0]));
  //  expect(eq(flexDiffRight[1], flexDiffMatrixRight[1]));
  //  expect(eq(flexDiffRight[2], flexDiffMatrixRight[2]));
  //  expect(eq(flexDiffRight[3], flexDiffMatrixRight[3]));
  //};
};


int main(int argc, char **argv) {
  if (argc > 1) {
    boost::ut::cfg<boost::ut::override> = {.filter = argv[1]};
  }
  return boost::ut::cfg<boost::ut::override>.run();
}
