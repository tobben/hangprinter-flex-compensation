#pragma once

#include <Matrix.h>
#include <array>
#include <iomanip>

using Point = std::array<float, 3>;
static Point cross(Point a, Point b) { return {a[1] * b[2] - a[2] * b[1], a[2] * b[0] - a[0] * b[2], a[0] * b[1] - a[1] * b[0]}; }
static float dot(Point a, Point b) { return a[0] * b[0] + a[1] * b[1] + a[2] * b[2]; }
static float norm(Point a) { return sqrtf(dot(a, a)); }
static Point operator-(Point const a, Point const b) { return {a[0] - b[0], a[1] - b[1], a[2] - b[2]}; }
static Point operator-(Point const a) { return {-a[0], -a[1], -a[2]}; }
static Point operator+(Point const a, Point const b) { return {a[0] + b[0], a[1] + b[1], a[2] + b[2]}; }
static Point operator/(Point const a, float const b) { return {a[0] / b, a[1] / b, a[2] / b}; }
static Point operator*(float const b, Point const a) { return {a[0] * b, a[1] * b, a[2] * b}; }
static Point operator*(Point const a, float const b) { return {a[0] * b, a[1] * b, a[2] * b}; }


constexpr size_t HANGPRINTER_MAX_ANCHORS = 5;
constexpr size_t X_AXIS = 0;
constexpr size_t Y_AXIS = 1;
constexpr size_t Z_AXIS = 2;
constexpr size_t A_AXIS = 0;
constexpr size_t B_AXIS = 1;
constexpr size_t C_AXIS = 2;
constexpr size_t D_AXIS = 3;
constexpr size_t I_AXIS = 4;

constexpr float Pi = 3.14159265358979323846;

constexpr float fsquare(float const a) { return a * a; }

static inline float fastSqrtf(float const x) { return sqrtf(x); }

static inline float hyp3(std::array<float, 3> const a, std::array<float, 3> const b) {
	return fastSqrtf(fsquare(a[2] - b[2]) + fsquare(a[1] - b[1]) + fsquare(a[0] - b[0]));
}

constexpr float max(float const a, float const b) {
  if (a > b)
    return a;
  return b;
}
constexpr float min(float const a, float const b) {
  if (a < b)
    return a;
  return b;
}

namespace {
std::ostream &operator<<(std::ostream &o, std::array<auto, 4> const &a) {
  return o << "[ " << a[0] << ", " << a[1] << ", " << a[2] << ", " << a[3] << " ], ";
}

std::ostream &operator<<(std::ostream &o, std::array<auto, 5> const &a) {
  return o << "[ " << a[0] << ", " << a[1] << ", " << a[2] << ", " << a[3] << ", " << a[4] << " ], ";
}

std::ostream &operator<<(std::ostream &o, Point const &a) {
  return o << "[" << std::setw(5) << a[0] << ", " << std::setw(5) << a[1] << ", " << std::setw(5) << a[2] << "], ";
}

template<typename T, size_t ROWS, size_t COLS> std::ostream &operator<<(std::ostream &o, FixedMatrix<T, ROWS, COLS> const &M) {
  for (int i = 0; i < M.rows(); ++i) {
    o << "[ ";
    for (int j = 0; j < M.cols(); ++j) {
      if (j != 0) {
        o << ", ";
      }
      o << std::setw(8) << std::fixed << std::setprecision(4) << std::right << M(i, j);
    }
    o << " ]";
    if (i != M.rows() - 1) {
      o << ", ";
    }
    o << '\n';
  }
  return o;
}
} // namespace
