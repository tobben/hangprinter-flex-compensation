
//intersection(){
  polyhedron( points = [ [-1000.0, 1000.0, -121.280], [500.0, -500.0, -131.53], [-1415.73, 707.61, -121.82], [0.0, 0.0, 2299.83] ], faces = [ [0, 1, 2], [0, 1, 3], [0, 2, 3 ], [1, 2, 3] ], convexity = 1);

  rad = 0.003;
  color("green")
    translate([-0.1,0,0])
      sphere(r=rad);
  color("black")
    translate([0.0,0,0])
      sphere(r=rad);
  color("white")
    translate([0.1,0,0])
      sphere(r=rad);
//}
